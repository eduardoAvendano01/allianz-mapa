Instalación e invocación del componente

1. Generar llave de Google Maps API y sustituir el valor de APIKEY en index.html

2. Copiar carpeta www en el portlet de Liferay

3. Agregar los imports de los archivos de la carpeta www como se muestra en el archivo index.html

4. Realizar la invocación del componente con la siguiente línea de código:

LibMapaAllianz.obtenerDireccionMapa();